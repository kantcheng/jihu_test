测试一：为该服务开发Dockerfile，构建容器镜像并发布到镜像仓库（地址不限）  


[root@localhost ~]# cat Dockerfile   
FROM docker.io/centos   
FROM golang:1.17 AS builder  
ENV GOPROXY=https://goproxy.cn  
ENV GO111MODULE=on  
RUN  apt-get install -y git  
RUN git clone https://gitlab.cn/jihulab/jh-infra/hands-on-tests/service-a.git  
WORKDIR /go/service-a  
RUN go mod tidy  
RUN go build   
EXPOSE 8080  
RUN sed -i 's/8888/8080/g' config.yaml  
ENTRYPOINT ["./service-a", "--file", "config.yaml"]  
[root@localhost ~]#   

[root@localhost ~]# docker build -t chengchangqian/jihu:v1 .  
Sending build context to Docker daemon  8.356MB  
Sending build context to Docker daemon  369.6MB  
Step 1/12 : FROM docker.io/centos  
 ---> 5d0da3dc9764  
Step 2/12 : FROM golang:1.17 AS builder  
 ---> 37eabbc422cd  
Step 3/12 : ENV GOPROXY=https://goproxy.cn  
 ---> Using cache  
 ---> b6eab76497a6  
Step 4/12 : ENV GO111MODULE=on  
 ---> Using cache  
 ---> 170b90cfaacc  
Step 5/12 : RUN  apt-get install -y git  
 ---> Using cache  
 ---> 1f819c4c4378  
Step 6/12 : RUN git clone https://gitlab.cn/jihulab/jh-infra/hands-on-tests/service-a.git  
 ---> Using cache  
 ---> bfefe9734113  
Step 7/12 : WORKDIR /go/service-a  
 ---> Using cache  
 ---> 02e8be6b03ce  
Step 8/12 : RUN go mod tidy  
 ---> Using cache  
 ---> 05202618d0a8  
Step 9/12 : RUN go build  
 ---> Using cache  
 ---> 3a0d0e06a014  
Step 10/12 : EXPOSE 8080  
 ---> Using cache  
 ---> 21c5e044417a  
Step 11/12 : RUN sed -i 's/8888/8080/g' config.yaml  
 ---> Using cache  
 ---> d3abce58d125  
Step 12/12 : ENTRYPOINT ["./service-a", "--file", "config.yaml"]  
 ---> Using cache  
 ---> bf03cdc87127  
Successfully built bf03cdc87127  
Successfully tagged chengchangqian/jihu:v1  
[root@localhost ~]#   

镜像放到docker hub里面了。
https://registry.hub.docker.com/repository/docker/chengchangqian/jihu


测试二：⽤Terraform代码实现创建腾讯云TKE（实现代码即可）  

代码在tke目录下的main.tf    
kube.config 是访问配置文件。  






测试三：开发Helm chart，将该服务部署到部署到指定的TKE集群（集群访问⽅式将以附件抄送）  

代码在helm目录下。    
提交日志  
[root@localhost helm]# helm package service-a  
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /root/.kube/config   
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /root/.kube/config  
Successfully packaged chart and saved it to: /root/helm/service-a-0.1.0.tgz  
[root@localhost helm]# helm install service-a service-a-0.1.0.tgz   
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /root/.kube/config  
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /root/.kube/config  
NAME: service-a  
LAST DEPLOYED: Sun Dec 12 03:57:40 2021  
NAMESPACE: default  
STATUS: deployed  
REVISION: 1  
NOTES:  
1. Get the application URL by running these commands:  
  export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=service-a,app.kubernetes.io/instance=service-a" -o jsonpath="{.items[0].metadata.name}")  
  echo "Visit http://127.0.0.1:8080 to use your application"  
  kubectl --namespace default port-forward $POD_NAME 8080:80  
[root@localhost helm]# export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=service-a,app.kubernetes.io/instance=service-a" -o jsonpath="{.items[0].metadata.name}")    
[root@localhost helm]# echo "Visit http://127.0.0.1:8080 to use your application"  
Visit http://127.0.0.1:8080 to use your application  
   

这里需要注意的是我映射到本地的8080端口  
  
^C[root@localhost helm]# kubectl --namespace default port-forward $POD_NAME 8080:8080  
Forwarding from 127.0.0.1:8080 -> 8080  
Forwarding from [::1]:8080 -> 8080  
Handling connection for 8080  
Handling connection for 8080  
Handling connection for 8080  
Handling connection for 8080  
Handling connection for 8080  
  
  
测试结果  
[root@localhost ~]# curl 127.0.0.1:8080/api/v1/features  
{"Env":"test","Port":"8080","FeatureOne":true,"FeatureTwo":false}[root@localhost ~]#   
[root@localhost ~]# curl 127.0.0.1:8080/healthz  
{"message":"Service a is running well","status":"OK"}[root@localhost ~]#   

