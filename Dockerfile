FROM docker.io/centos
FROM golang:1.17 AS builder
ENV GOPROXY=https://goproxy.cn
ENV GO111MODULE=on
RUN  apt-get install -y git
RUN git clone https://gitlab.cn/jihulab/jh-infra/hands-on-tests/service-a.git
WORKDIR /go/service-a
RUN go mod tidy
RUN go build 
EXPOSE 8080
RUN sed -i 's/8888/8080/g' config.yaml
ENTRYPOINT ["./service-a", "--file", "config.yaml"]
